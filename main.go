package main

import (
	"context"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/sensorbucket/datalink/pkg/flagenv"
	"gitlab.com/sensorbucket/datalink/pkg/keystore"
	"gitlab.com/sensorbucket/datalink/pkg/mqses"
	"gitlab.com/sensorbucket/datalink/service"
	"gitlab.com/sensorbucket/datalink/service/adapters/amqpl"
	"gitlab.com/sensorbucket/datalink/service/adapters/http"
	"gitlab.com/sensorbucket/datalink/service/adapters/mgmtstore"
)

var (
	httpAddress    = flagenv.String("ADDRESS", "address", "0.0.0.0", "Set the http listening address")
	httpPort       = flagenv.Int("PORT", "port", 3000, "Set the http listening port")
	privateKeyPath = flagenv.String("KEY_PATH", "keypath", "./private.key", "Use a local private key file instead of JWKS URI")
	jwksURI        = flagenv.String("JWKS_VERIFICATION", "jwks", "", "Set the uri to a JSON Web Key Set")
	amqpURI        = flagenv.String("AMQP_URI", "amqp", "", "Connection string for the AMQP server")
	amqpExchange   = flagenv.String("AMQP_EXCHANGE", "amqp-exchange", "datalink/messages", "The AMQP exchange to publish uplink messages on")
	managementURI  = flagenv.String("MANAGEMENT_URI", "management-uri", "http://sb-management", "Base URI to the management service")
	syncInterval   = flagenv.Int("SYNC_INTERVAL", "sync-interval", 60, "The amount of seconds between sources/device-types synchronization")
)

var log = logrus.New()

func createKeystore() keystore.Keyprovider {
	if *jwksURI != "" {
		ks := keystore.NewJWKSKeystore(*jwksURI)
		// Fetch keys to check connection
		err := ks.Update()
		if err != nil {
			log.WithError(err).Fatal("Could not fetch JWKS!")
		}
		return ks
	} else if *privateKeyPath != "" {
		// If no JWKSURI is set then a local file MUST be set
		// Read keyfile, assume it is RSA private key
		dat, err := ioutil.ReadFile(*privateKeyPath)
		if err != nil {
			log.Fatal(err)
		}
		b, _ := pem.Decode(dat)
		key, err := x509.ParsePKCS1PrivateKey(b.Bytes)
		if err != nil {
			log.Fatal(err)
		}

		ks := keystore.NewStaticKeystore()
		ks.Add("static", key)
		return ks
	} else {
		log.Fatal("Cannot start without JWKS uri or private key file set")
		return nil
	}
}

func main() {
	// Used to stop goroutines
	ctx, cancel := context.WithCancel(context.Background())
	errc := make(chan error, 1)
	// Catches interrupt signals from OS
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGABRT)

	// Create dependencies for service
	ks := createKeystore()
	mq := mqses.New(ctx, log, *amqpURI)
	store := mgmtstore.New(ctx, *managementURI, ks)
	go store.Start(time.Duration(*syncInterval) * time.Second)
	pipeline := amqpl.New(mq, *amqpExchange)
	svc := service.New(store, pipeline)
	http := http.New(ctx, log, svc, ks)

	// http server is blocking so start in a gorotuine
	go func() {
		// Start http server
		if err := http.StartListening(fmt.Sprintf("%s:%d", *httpAddress, *httpPort)); err != nil {
			errc <- err
		}
	}()

	// Wait for either an OS interrupt signal or an error
	select {
	case <-sigChan:
	case err := <-errc:
		log.WithError(err).Error("Fatal error occured")
	}
	log.Infof("Shutting down")
	cancel()
}
