module gitlab.com/sensorbucket/datalink

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v1.5.1
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.7.0 // indirect
	gitlab.com/sensorbucket/go-worker v0.0.2-0.20210512095146-717c08f812f0
)
