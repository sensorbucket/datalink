package service

import "net/http"

var (
	// ErrSourceNotFound ...
	ErrSourceNotFound = &ClientError{
		Status:  http.StatusNotFound,
		Message: "source could not be found",
	}

	// ErrDeviceTypeNotFound ...
	ErrDeviceTypeNotFound = &ClientError{
		Status:  http.StatusNotFound,
		Message: "deviceType could not be found",
	}
)

// ClientError ...
type ClientError struct {
	Status  int
	Message string
	cause   error
}

func (err *ClientError) Error() string {
	if err.cause != nil {
		return err.cause.Error()
	}
	return err.Message
}
