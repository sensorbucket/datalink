package service

import (
	"context"
	"time"

	"github.com/gofrs/uuid"
	"gitlab.com/sensorbucket/go-worker/pipeline"
)

// logic ...
type logic struct {
	store    Store
	pipeline Pipeline
}

func New(store Store, pl Pipeline) Service {
	return &logic{
		store:    store,
		pipeline: pl,
	}
}

func (l *logic) CreateUplink(ctx context.Context, src, devtyp string, payload interface{}) (*UplinkReceipt, error) {
	oID, _ := ctx.Value(CTXOrganisationKey).(int)
	token, _ := ctx.Value(CTXTokenKey).(string)
	uuid, _ := uuid.NewV4()
	id := uuid.String()

	// Ensure that the source and device-type exists
	exist, err := l.store.DeviceTypeExists(devtyp)
	if err != nil {
		return nil, err
	}
	if !exist {
		return nil, ErrDeviceTypeNotFound
	}
	exist, err = l.store.SourceExists(src)
	if err != nil {
		return nil, err
	}
	if !exist {
		return nil, ErrSourceNotFound
	}

	// Build the message
	msg := &pipeline.NodeMessage{
		ID:         id,
		Direction:  pipeline.UplinkMessage,
		Type:       pipeline.SourceMessage,
		DateTime:   time.Now(),
		Source:     src,
		DeviceType: devtyp,
		Owner:      oID,
		Token:      token,
		Payload:    payload,
	}

	// Publish the message to the queue
	err = l.pipeline.Publish(msg)
	if err != nil {
		return nil, err
	}

	// Return the receipt
	return &UplinkReceipt{
		ID: id,
	}, nil
}
