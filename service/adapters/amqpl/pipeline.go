package amqpl

import (
	"encoding/json"

	"github.com/streadway/amqp"
	"gitlab.com/sensorbucket/datalink/pkg/mqses"
	"gitlab.com/sensorbucket/datalink/service"
	"gitlab.com/sensorbucket/go-worker/pipeline"
)

// pl ...
type pl struct {
	mq   *mqses.Session
	xchg string
}

func New(mq *mqses.Session, xchg string) service.Pipeline {
	return &pl{
		mq:   mq,
		xchg: xchg,
	}
}

func (p *pl) Publish(nm *pipeline.NodeMessage) error {
	jsonMSG, err := json.Marshal(nm)
	if err != nil {
		return err
	}

	err = p.mq.Channel().Publish(
		p.xchg,
		nm.RoutingKey(),
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        jsonMSG,
		},
	)
	return err
}
