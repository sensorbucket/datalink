package mgmtstore

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/sensorbucket/datalink/pkg/keystore"
)

var (
	errManagementServiceError = errors.New("management service returned an error status code")
)

// MGMTStore ...
type MGMTStore struct {
	ctx         context.Context
	url         string
	sources     []string
	deviceTypes []string
	keystore    keystore.Keyprovider
}

func New(ctx context.Context, url string, ks keystore.Keyprovider) *MGMTStore {
	s := &MGMTStore{
		ctx:         ctx,
		url:         url,
		keystore:    ks,
		sources:     make([]string, 0),
		deviceTypes: make([]string, 0),
	}

	return s
}

// SourceExists checks whether the source w/ id exists
func (s *MGMTStore) SourceExists(id string) (bool, error) {
	for _, v := range s.sources {
		if id == v {
			return true, nil
		}
	}
	return false, nil
}

// DeviceTypeExists checks whether the source w/ id exists
func (s *MGMTStore) DeviceTypeExists(id string) (bool, error) {
	for _, v := range s.deviceTypes {
		if id == v {
			return true, nil
		}
	}
	return false, nil
}

func (s *MGMTStore) createServiceToken() (string, error) {
	key := s.keystore.GetLatest()
	t := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.StandardClaims{
		Audience:  "service",
		Issuer:    "datalink",
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
	})
	t.Header["kid"] = key.ID

	return t.SignedString(key.RSAPrivateKey())
}

// Update updates the source and device-type IDs
func (s *MGMTStore) Update() error {
	auth, err := s.createServiceToken()
	if err != nil {
		return err
	}

	// Build request
	req, err := http.NewRequest("GET", s.url+"/device-types", nil)
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "bearer "+auth)

	// Do request
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	if res.StatusCode < 200 || res.StatusCode > 299 {
		return errManagementServiceError
	}

	var typs apiResponse
	err = json.NewDecoder(res.Body).Decode(&typs)
	if err != nil {
		return err
	}

	// Get sources
	req, err = http.NewRequest("GET", s.url+"/sources", nil)
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "bearer "+auth)

	res, err = http.DefaultClient.Do(req)
	if res.StatusCode < 200 || res.StatusCode > 299 {
		return errManagementServiceError
	}

	var srcs apiResponse
	err = json.NewDecoder(res.Body).Decode(&srcs)
	if err != nil {
		return err
	}

	// set values
	s.deviceTypes = make([]string, len(typs.Data))
	for ix, v := range typs.Data {
		s.deviceTypes[ix] = v.ID
	}
	s.sources = make([]string, len(srcs.Data))
	for ix, v := range srcs.Data {
		s.sources[ix] = v.ID
	}

	return nil
}

// Start periodically synchronizes the ids
func (s *MGMTStore) Start(i time.Duration) {
	err := s.Update()
	if err != nil {
		logrus.Printf("store error occured: %s", err)
	}

	t := time.NewTicker(i)
	for {
		select {
		// If the context is cancelled then stop the ticker
		case <-s.ctx.Done():
			t.Stop()
		// On every tick, synchronize the store
		case <-t.C:
			err := s.Update()
			if err != nil {
				logrus.Printf("store error occured: %s", err)
				continue
			}
			logrus.Printf("store succesfully updated")
		}
	}
}
