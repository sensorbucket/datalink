package http

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
	"gitlab.com/sensorbucket/datalink/service"
)

var (
	JWT_AUDIENCE = "source"
)

func (a *HTTPAdapter) routes() {
	r := a.router
	auth := a.auth()

	r.Use(auth)
	r.Post("/uplink/{source}/{devicetype}", a.createUplink())
}

func (a *HTTPAdapter) createUplink() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		src := chi.URLParam(r, "source")
		dev := chi.URLParam(r, "devicetype")

		// Cannot directly cast the r.Header field
		// so we iterate it to create our own
		hdrs := map[string]string{}
		for n := range (map[string][]string)(r.Header) {
			hdrs[n] = r.Header.Get(n)
		}

		// Read the body
		body := map[string]interface{}{}
		json.NewDecoder(r.Body).Decode(&body)

		// Create the uplink payload
		payload := &service.HTTPPayload{
			Headers: hdrs,
			Body:    body,
		}

		// Create the uplink message
		dto, err := a.svc.CreateUplink(ctx, src, dev, payload)
		if err != nil {
			a.log.WithError(err).Warn("Failed to create uplink")
			respondErr(rw, err)
			return
		}

		// Respond with the ID for tracking
		respond(rw, &response{
			Status:  http.StatusAccepted,
			Message: "Uplink message accepted",
			Data:    dto,
		})
	}
}

// ====================
// helper
// ====================

func respond(rw http.ResponseWriter, r *response) {
	rw.Header().Add("content-type", "application/json")
	rw.WriteHeader(r.Status)
	enc := json.NewEncoder(rw)

	err := enc.Encode(r)
	if err != nil {
		enc.Encode(map[string]interface{}{
			"message": "Response could not be shown",
		})
	}
}

func respondErr(rw http.ResponseWriter, origin error) {
	switch err := origin.(type) {
	case *service.ClientError:
		respond(rw, &response{
			Status:  err.Status,
			Message: err.Message,
		})
	default:
		respond(rw, errInternal)
	}
}

// ====================
// Auth middleware
// ====================

func (a *HTTPAdapter) auth() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			// Extract JWT key string from request
			key, err := extractKey(r)
			if err != nil {
				a.log.WithError(err).Error("Authentication failed")
				respond(rw, errUnauthorized)
				return
			}

			// Verify key
			t, err := jwt.ParseWithClaims(key, &jwtClaims{}, func(t *jwt.Token) (interface{}, error) {
				kid, ok := t.Header["kid"].(string)
				if !ok || kid == "" {
					return nil, errors.New("token is missing KID header")
				}

				key := a.keystore.Get(kid)
				if key == nil {
					return nil, errors.New("Key with KID not found")
				}
				return key.RSAPublicKey(), err
			})
			if err != nil {
				a.log.WithError(err).Warn("Authentication failed")
				respond(rw, errUnauthorized)
				return
			}

			// Extract org id from claim
			claims := t.Claims.(*jwtClaims)

			// Assert audience
			if claims.Audience != JWT_AUDIENCE {
				a.log.Warn("JWT key has invalid audience claim")
				respond(rw, errUnauthorized)
			}

			r = r.WithContext(context.WithValue(
				r.Context(),
				service.CTXOrganisationKey,
				claims.OrganisationID,
			))
			r = r.WithContext(context.WithValue(
				r.Context(),
				service.CTXTokenKey,
				key,
			))

			// Call next handler
			next.ServeHTTP(rw, r)
		})
	}
}

func extractKey(r *http.Request) (string, error) {
	hdr := r.Header.Get("authorization")
	parts := strings.Split(hdr, " ")

	// Requires two parts: <type> <token>
	if len(parts) < 2 {
		return "", errors.New("invalid authorization header")
	}

	// Type must be bearer
	if strings.ToLower(parts[0]) != "bearer" {
		return "", errors.New("only bearer token authorization is allowed")
	}

	return parts[1], nil
}
