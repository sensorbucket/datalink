package http

import (
	"context"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/sensorbucket/datalink/pkg/keystore"
	"gitlab.com/sensorbucket/datalink/service"
)

// KeyStore ...
type KeyStore interface {
	GetLatest() *keystore.Key
	Get(kid string) *keystore.Key
}

// response ...
type response struct {
	Status  int         `json:"-"`
	Message string      `json:"message,omitempty"`
	Error   error       `json:"-"`
	Data    interface{} `json:"data,omitempty"`
}

// jwtClaims ...
type jwtClaims struct {
	jwt.StandardClaims
	OrganisationID int `json:"org,omitempty"`
}

// HTTPAdapter ...
type HTTPAdapter struct {
	ctx      context.Context
	log      *logrus.Logger
	svc      service.Service
	router   chi.Router
	keystore KeyStore
}

func New(ctx context.Context, log *logrus.Logger, svc service.Service, ks KeyStore) *HTTPAdapter {
	a := &HTTPAdapter{
		ctx:      ctx,
		log:      log,
		svc:      svc,
		router:   chi.NewMux(),
		keystore: ks,
	}
	a.routes()

	return a
}

func (a *HTTPAdapter) StartListening(addr string) error {
	server := &http.Server{
		Addr:         addr,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		Handler:      a.router,
	}

	go func() {
		// Blocks until context is done
		<-a.ctx.Done()
		a.log.Info("Closing HTTP server")

		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		defer cancel()
		server.Shutdown(ctx)
	}()

	// Start serving, blocking!
	a.log.Infof("HTTP server starting on %s", addr)
	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		a.log.WithError(err).Error("HTTP Server errored")
		return err
	}

	return nil
}

func (a *HTTPAdapter) Routes() chi.Router {
	return a.router
}
