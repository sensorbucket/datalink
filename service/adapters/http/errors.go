package http

import "net/http"

var (
	errUnauthorized = &response{
		Status:  http.StatusUnauthorized,
		Message: "unauthorized",
	}
	errInternal = &response{
		Status:  http.StatusInternalServerError,
		Message: "Unknown error occured",
	}
)
