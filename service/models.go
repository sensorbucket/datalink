package service

// UplinkReceipt contains information about the submitted uplink packet
type UplinkReceipt struct {
	ID string `json:"id"`
}

// HTTPPayload represents a message payload
type HTTPPayload struct {
	Headers map[string]string `json:"headers"`
	Body    interface{}       `json:"body"`
}
