package service

import (
	"context"

	"gitlab.com/sensorbucket/go-worker/pipeline"
)

type ctxOrganisationKey string

var CTXOrganisationKey ctxOrganisationKey = "organisation_key"

var CTXTokenKey = struct{}{}

// Service ...
type Service interface {
	CreateUplink(ctx context.Context, src, devtyp string, payload interface{}) (*UplinkReceipt, error)
}

// Store ...
type Store interface {
	DeviceTypeExists(id string) (bool, error)
	SourceExists(id string) (bool, error)
}

// Pipeline ...
type Pipeline interface {
	Publish(*pipeline.NodeMessage) error
}
