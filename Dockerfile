FROM golang:alpine AS builder

WORKDIR /opt

# Install dependencies first for caching purposes
COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download

# Copy source code and build
COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o ./bin/

#
# Scratch docker image containing nothing but the application
#
FROM alpine AS production
# Start at /opt
WORKDIR /opt
# Copy our static executable.
COPY --from=builder /opt/bin/datalink /opt/datalink
# Expose ports
EXPOSE 3000
# Run the hello binary.
CMD ["/opt/datalink"]
