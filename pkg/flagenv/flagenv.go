package flagenv

import (
	"flag"
	"log"
	"os"
	"strconv"
)

// String get a parameter from the environment or from a command line flag
func String(env, flg, fallback, usage string) *string {
	fval := flag.String(flg, fallback, usage)
	eval := os.Getenv(env)
	if (fval == nil || *fval == fallback) && eval != "" {
		return &eval
	}
	return fval
}

// Int get a parameter from the environment or from a command line flag
func Int(env, flg string, fallback int, usage string) *int {
	fval := flag.Int(flg, fallback, usage)
	evalStr := os.Getenv(env)
	if (fval == nil || *fval == fallback) && evalStr != "" {
		strint, err := strconv.ParseInt(evalStr, 10, 64)
		if err != nil {
			log.Printf("WARNING: Environment variable '%s' could not be converted to an integer", env)
		} else {
			eval := int(strint)
			return &eval
		}
	}
	return fval
}
