package mqses

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// ChanSetupFn is a function that is called when a new channel must be set up
type ChanSetupFn func(c *amqp.Channel) error

const (
	reconnectDelay = 5 * time.Second
	maxRetries     = 5
)

// DeclarationFunc is fired on a new connection
// if this function fails, it assumes the reconnection has failed
type DeclarationFunc func(*amqp.Connection, *amqp.Channel) error

// Status represents the current status of the message queue connection
type Status int

const (
	// StatusConnected means a connection to the MQ is present
	StatusConnected Status = iota
	// StatusDisconnected means the connection is lost
	StatusDisconnected
)

// Session ...
type Session struct {
	ctx             context.Context
	log             *logrus.Logger
	connection      *amqp.Connection
	channel         *amqp.Channel
	notifyClose     chan *amqp.Error
	notifyConfirm   chan amqp.Confirmation
	isConnected     bool
	declarationFunc DeclarationFunc
}

// New ...
func New(ctx context.Context, log *logrus.Logger, addr string) *Session {
	s := &Session{
		ctx: ctx,
		log: log,
	}

	go s.handleReconnect(addr)

	return s
}

// SetDeclaration sets the function that will assert the topology
func (s *Session) SetDeclaration(fn DeclarationFunc) {
	s.declarationFunc = fn
}

func (s *Session) handleReconnect(addr string) {
	for {
		s.isConnected = false
		s.log.Info("Attempting AMQP reconnect")

		// Attempt reconnection until
		retryCount := 0
		for retryCount < maxRetries && !s.connect(addr) {
			// During the reconnect delay we might close the connection
			// so catch <-s.ctx.Done()
			select {
			// Fired when closing
			case <-s.ctx.Done():
				s.close()
				return

			// Fires after x seconds
			case <-time.After(reconnectDelay + time.Duration(retryCount)*time.Second):
				retryCount++
				s.log.WithField("retry", retryCount).Info("Attempting reconnects")
			}
		}

		// If we have not returned yet then we are connected
		s.isConnected = true
		s.log.Info("AMQP Reconnected")

		// Wait for exit or error
		select {
		case <-s.ctx.Done():
			s.close()
		case <-s.notifyClose:
		}
	}
}

// Connect ...
func (s *Session) connect(addr string) bool {
	conn, err := amqp.Dial(addr)
	if err != nil {
		s.log.WithError(err).Warn("AMQP Connection failed")
		return false
	}

	ch, err := conn.Channel()
	if err != nil {
		s.log.WithError(err).Warn("AMQP Channel creation failed")
		return false
	}

	if s.declarationFunc != nil {
		err = s.declarationFunc(conn, ch)
		if err != nil {
			s.log.WithError(err).Warn("AMQP Declaration failed")
			return false
		}
	}

	s.changeConnection(conn, ch)
	s.isConnected = true
	return true
}

func (s *Session) changeConnection(conn *amqp.Connection, ch *amqp.Channel) {
	s.connection = conn
	s.channel = ch
	s.notifyClose = make(chan *amqp.Error)
	s.notifyConfirm = make(chan amqp.Confirmation)
	ch.NotifyClose(s.notifyClose)
	ch.NotifyPublish(s.notifyConfirm)
}

// Close the session
func (s *Session) close() {
	if err := s.channel.Close(); err != nil {
		s.log.WithError(err).Warn("AMQP Channel close failed")
	}
	if err := s.connection.Close(); err != nil {
		s.log.WithError(err).Warn("AMQP Connection close failed")
	}
}

// Channel returns the active channel
func (s *Session) Channel() *amqp.Channel {
	return s.channel
}

func (s *Session) IsConnected() bool {
	return s.isConnected
}
