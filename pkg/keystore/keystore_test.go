package keystore_test

import (
	"testing"

	"gitlab.com/sensorbucket/datalink/pkg/keystore"
)

func TestGetPublicKey(t *testing.T) {
	pk := mkPrivateKey()
	key := &keystore.Key{
		Content: &pk.PublicKey,
	}

	if key.RSAPublicKey() != &pk.PublicKey {
		t.Error("Key.RSAPublicKey does not return correct key")
	}
}

func TestGetPrivateKey(t *testing.T) {
	pk := mkPrivateKey()
	key := &keystore.Key{
		Content: pk,
	}

	if key.RSAPrivateKey() != pk {
		t.Error("Key.RSAPrivateKey does not return correct key")
	}
}

func TestGetPublicKeyWithPrivateKey(t *testing.T) {
	pk := mkPrivateKey()
	key := &keystore.Key{
		Content: pk,
	}

	if key.RSAPublicKey() != &pk.PublicKey {
		t.Error("Key.RSAPublicKey does not return correct key if the Key content is an RSA private key")
	}
}
