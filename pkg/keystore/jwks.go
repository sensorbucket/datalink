package keystore

import (
	"context"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"time"
)

// JWKSKeystore fetches keys from a jwks uri
type JWKSKeystore struct {
	uri    string
	jwks   JSONWebKeySet
	jwkmap map[string]*JSONWebKey
}

// NewJWKSKeystore creates a new keystore
// call #Update() to update the current keysets or call #Start() to periodically
// update the keys
func NewJWKSKeystore(uri string) *JWKSKeystore {
	return &JWKSKeystore{
		uri:    uri,
		jwks:   JSONWebKeySet{},
		jwkmap: map[string]*JSONWebKey{},
	}
}

// StartPeriodicUpdate periodically updates the keyset
func (k *JWKSKeystore) StartPeriodicUpdate(i time.Duration) (context.Context, context.CancelFunc) {
	// Create the ticker and the context
	t := time.NewTicker(i)
	ctx, cancel := context.WithCancel(context.Background())

	// Go routine loops until context is canceled.
	// otherwise every tick this will update the keys
	go func() {
		for {
			select {
			// When the context is cancelled this channel fires and exits the
			// goroutine
			case <-ctx.Done():
				log.Printf("Stopping keystore updater")
				return
			// This channel is fired every ticker tick
			case <-t.C:
				log.Printf("Updating keystore...")
				err := k.Update()
				if err != nil {
					log.Printf("Error updating keyset: %v", err)
				} else {
					log.Printf("Keystore succesfully updated")
				}
			}
		}
	}()

	return ctx, cancel
}

// Update the keyset to new keys
func (k *JWKSKeystore) Update() error {
	res, err := http.DefaultClient.Get(k.uri)
	if err != nil {
		return err
	}

	// Important that k.jwks is initialized otherwise it is an empty pointer
	// and this will throw a json decode error
	var jwks JSONWebKeySet
	err = json.NewDecoder(res.Body).Decode(&jwks)
	if err != nil {
		return err
	}

	// Removing keys would cause a lot to fail, so as a failsafe don
	if len(jwks.Keys) < 1 {
		return errors.New("newly fetched jwks has no keys, refusing to update")
	}

	jwkmap := map[string]*JSONWebKey{}
	for _, jwk := range jwks.Keys {
		jwkmap[jwk.KeyID] = &jwk
	}

	// Update keystore values
	k.jwks = jwks
	k.jwkmap = jwkmap

	return nil
}

// =======================================================
//  Keystore interface implementation
// =======================================================

// Get returns a JSON Web Key with the given key ID
func (k *JWKSKeystore) Get(kid string) *Key {
	jwk := k.jwkmap[kid]
	if jwk == nil {
		return nil
	}

	return &Key{
		Content: jwk.Key,
		ID:      jwk.KeyID,
	}
}

// GetLatest returns the latest key
func (k *JWKSKeystore) GetLatest() *Key {
	// The first key in the ordered list of keys is the newest
	jwk := &k.jwks.Keys[0]
	if jwk == nil {
		return nil
	}

	return &Key{
		Content: jwk.Key,
		ID:      jwk.KeyID,
	}
}
