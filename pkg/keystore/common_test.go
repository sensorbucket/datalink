package keystore_test

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
)

func mkPrivateKey() *rsa.PrivateKey {
	b, _ := pem.Decode([]byte(`-----BEGIN RSA PRIVATE KEY-----
MIIBOgIBAAJBAJioyRBODoSKPEQ92QPrtvN/DM9t5HKqFOo9+6uSGSGOMrr1QLqr
roMR9nK9bRDxAPc4FMrNPljwLtP1MM1r/mMCAwEAAQJAXGfevSXil8vtSwl88Wif
J6lCIdVNMTNO0bOPQX2ABNTU/tVY/pacR58zKy1SHJiMkb1+KHBhuuNJdrU9O6Es
0QIhAMRMTyhyZtwqNgVDN2jtneiTI4ekGDwtU15Tw0IIbLYXAiEAxxbHf3hSe+ky
FnhTZsuvck76UCsktoRtqjHCAhJu9ZUCIG/f6dFsehEKgaU4LNFBc5jEmyBINoO9
dWEyLftpX8PbAiEAlHLc0KSERJauXWjdL2IjcUWIieyRlHKMXwJ5Ghhqmj0CIAyK
98Ak9qRHQTNZD31PWyTCnjI2VZnyXQUTNe1v3Tw+
-----END RSA PRIVATE KEY-----`))
	pk, _ := x509.ParsePKCS1PrivateKey(b.Bytes)
	return pk
}
